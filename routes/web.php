<?php
include_once 'bootstrap/Routing/Request.php';
include_once 'bootstrap/Routing/Route.php';
include_once 'bootstrap/AutoLoad.php';
$router = new Route(new Request);

$router->get('/', function () {
    Load::controller('test','index',null);
});

$router->get('/profile', function($request) {
   Load::controller('test','haha',null);
});

$router->post('/data', function ($request) {
    return json_encode($request->getBody());
});