<?php 
	/**
	 * summary
	 */
	class Load
	{

	    public static function controller($controller,$method,$params)
	    {

			$path = "controllers/".$controller.".php";
			
			if(file_exists($path))
			{
				include $path;
				$object = new $controller;
				if(method_exists($object,$method))
				{
					$object->$method($params);
				}
				else
				{
					return '<h3>Not found method '.$method.' in controller '.$controller.' </h3>';
				}
			}
			else
			{
				return '<h3>Controller not found</h3>';
			}
	    }
	}
